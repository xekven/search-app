const path = require('path');

module.exports = {
  store: path.resolve(__dirname, 'src/store/'),
  reducers: path.resolve(__dirname, 'src/store/reducers/'),
  actions: path.resolve(__dirname, 'src/store/actions/'),
  utils: path.resolve(__dirname, 'src/utils/'),
  api: path.resolve(__dirname, 'src/api/'),
  app: path.resolve(__dirname, 'src/app/'),
  static: path.resolve(__dirname, 'src/static/'),
  hooks: path.resolve(__dirname, 'src/app/hooks'),
  components: path.resolve(__dirname, 'src/app/components/'),
  common: path.resolve(__dirname, 'src/app/common/'),
  views: path.resolve(__dirname, 'src/app/views/'),
  'package.json': path.resolve(__dirname, './package.json'),
};
