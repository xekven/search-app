import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';

import {
  Wrapper,
  Icon,
  Button,
  Input,
} from './styled';

const SearchBar = ({ initialValue = '', onSearch = (e) => console.log(e) }) => { // eslint-disable-line

  const [value, setValue] = useState(initialValue);

  const handleSetValue = useCallback(
    value => {
      setValue(value);
    }, 
    []
  );

  const handleSearch = useCallback(
    e => {
      const { value } = e.target;
      onSearch(value);
      handleSetValue(value);
    },
    [onSearch, handleSetValue]
  );


    
  return (
    <Wrapper>
      <Input value={value} onChange={handleSearch} />
      <Button>
        <Icon>search</Icon>
      </Button>
    </Wrapper>
  );
};

SearchBar.propTypes = {
  initialValue: PropTypes.string,
  onSearch: PropTypes.func,
};

export default SearchBar;
