import styled from '@emotion/styled';

import CommonIcon from '@material-ui/core/Icon';

const Wrapper = styled.div`
    color: rgba(0, 0, 0, 0.87);
    background-color: rgb(255, 255, 255);
    transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
    box-sizing: border-box;
    font-family: Roboto, sans-serif;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px;
    border-radius: 2px;
    height: 48px;
    display: flex;
    justify-content: space-between;
    /* margin: 0px auto; */
    max-width: 480px;
    width: 100%;
`;

const Icon = styled(CommonIcon)`

`;

const Button = styled.button`
    box-sizing: border-box;
    display: inline-block;
    font-family: Roboto, sans-serif;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    cursor: pointer;
    text-decoration: none;
    padding: 12px;
    outline: none;
    font-size: 0px;
    font-weight: inherit;
    position: relative;
    z-index: 1;
    overflow: visible;
    transition: transform 200ms cubic-bezier(0.4, 0, 0.2, 1) 0s;
    width: 48px;
    height: 48px;
    opacity: 0.54;
    background: none;
    cursor: default;
`;

const Input = styled.input`
    padding: 0px;
    position: relative;
    width: 100%;
    border: none;
    outline: none;
    background-color: rgba(0, 0, 0, 0);
    color: rgba(0, 0, 0, 0.87);
    cursor: inherit;
    font: inherit;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    height: 100%;
    margin: 0 16px;
`;



export {
  Wrapper,
  Icon,
  Button,
  Input,
};
