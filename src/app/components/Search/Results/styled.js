import styled from '@emotion/styled';

const List = styled.ul`

`;

const Item = styled.li`

`;

const Wrapper = styled.div`
    max-width: 480px;
    width: 100%;
    padding: 16px;
    box-sizing: border-box;
    margin: 32px 0;
    display: flex;
    flex: 1;
    max-height: 480px;
    border: 1px solid #dededeab;
    border-radius: 4px;
    overflow: scroll;
`;

export {
  List,
  Item,
  Wrapper,
};
