import React from 'react';
import PropTypes from 'prop-types';

import {
  List,
  Item,
  Wrapper,
} from './styled';

const Results = ({ data }) => (
  <Wrapper>
    <List>
      {data.map(d => (
        <Item key={d}>{d}</Item>
      ))}
    </List>
  </Wrapper>
);


Results.propTypes = {
  data: PropTypes.array,
};

export default Results;

