import React from 'react';

import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import GlobalStyling from 'common/GlobalStyling';

import configureStore from 'store';
import routes from 'static/routes';

const store = configureStore();

const App = () => (
  <>
    <GlobalStyling />
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          {routes.map(route => (
            <Route key={route.path} {...route} />
          ))}
        </Switch>
      </BrowserRouter>
    </Provider>
  </>
);

export default App;
