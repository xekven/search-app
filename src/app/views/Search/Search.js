import React, { useEffect, useCallback, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import { useHistory, useLocation } from 'react-router-dom';
import qs from 'query-string';
import { throttle } from 'lodash';

import GeneralLayout from 'common/GeneralLayout';
import PageTitle from 'common/PageTitle';
import Loader from 'common/Loader';

import SearchBar from 'components/Search/SearchBar';
import Pagination from 'components/Search/Pagination';
import Results from 'components/Search/Results';

import { getSearchResults } from 'actions/search';
import getApiErrorMessage from 'utils/getApiErrorMessage';

import {
  CenteredWrapper,
} from './styled';

const Search = () => {

  const { search } = useLocation();

  const initialSearchValue = useMemo(() => {
    return qs.parse(search).query ? qs.parse(search).query : '';
  }, [search]);

  const [searchQuery, setSearchQuery] = useState(initialSearchValue);

  const history = useHistory();


  const dispatch = useDispatch();
  // dispatch on mount redux action which will be picked up by saga
  useEffect(() => {
    dispatch(getSearchResults());
  }, []); //eslint-disable-line

  const handleSearch = useCallback(
    throttle(
      value => {
        history.push({ search: value ? qs.stringify({ query: value }) : '' });
        setSearchQuery(value);
      },
      1000
    ),
    []
  );

  const { data, error, pending } = useSelector(
    createSelector(
      state => state.search,
      search => ({
        data: search.data,
        error: search.error,
        pending: search.pending,
      })
    )
  );

  const searchData = useMemo(() => {
    if (!searchQuery) {
      return [];
    }
    return data.filter(d => d.indexOf(searchQuery) !== -1);
  }, [data, searchQuery]);


  if (pending) {
    return (
      <CenteredWrapper>
        <Loader />
      </CenteredWrapper>
    );
  }

  if (error) {
    return (
      <CenteredWrapper>
        <span>{getApiErrorMessage(error)}</span>
      </CenteredWrapper>
    );
  }


  return (
    <GeneralLayout>
      <PageTitle text="Search for data 🤖" />
      <SearchBar initialValue={initialSearchValue} onSearch={handleSearch} />
      <Results data={searchData} />
      <Pagination />
    </GeneralLayout>
  );};

export default Search;
