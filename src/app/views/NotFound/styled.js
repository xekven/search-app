import styled from '@emotion/styled';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    /* align-items: stretch; */
    flex: 1;
    justify-content: center;
    align-items: center;
    line-height: 32px;
`;

export {
  Wrapper,
};
