import styled from '@emotion/styled';

const Wrapper = styled.div`
    display: flex;
    flex: 1;
    max-width: 1024px;
    width: 100%;
    align-self: center;
    padding: 80px 0;
    flex-direction: column;
    align-items: center;
    /* justify-content: center; */
`;

export {
  Wrapper,
};
