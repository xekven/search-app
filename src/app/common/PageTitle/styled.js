import styled from '@emotion/styled';

const Element = styled.h1`
    padding: 16px 0;
`;

export { Element };
