import React from 'react';
import { Global } from '@emotion/core';

import plainStyles from './plainStyles';

export default () => <Global styles={plainStyles} />;
