import styled from '@emotion/styled';

import CircularProgress from '@material-ui/core/CircularProgress';

const Progress = styled(CircularProgress)`

`;

export {
  Progress,
};
