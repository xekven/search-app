import {
  GET_ALL_SEARCH_RESULTS_PENDING,
  GET_ALL_SEARCH_RESULTS_SUCCESS,
  GET_ALL_SEARCH_RESULTS_FAILED,
} from 'store/static';

const defaultState = {
  data: [],
  error: null,
  pending: false,
};

export const searchReducer = (state = defaultState, action = {}) => {
  const { type, payload } = action;
  switch (type) {
  case GET_ALL_SEARCH_RESULTS_PENDING: {
    return { ...state, error: false, pending: true };
  }
  case GET_ALL_SEARCH_RESULTS_SUCCESS: {
    return { ...state, data: payload, error: null, pending: false };
  }
  case GET_ALL_SEARCH_RESULTS_FAILED: {
    return { ...state, error: payload, pending: false };
  }
  default:
    return state;
  }
};

