import { GET_ALL_SEARCH_RESULTS } from 'store/static';

export const getSearchResults = () => ({ type: GET_ALL_SEARCH_RESULTS });
