import NotFound from 'views/NotFound';
import Search from 'views/Search';

export default [
  { exact: true, component: Search, path: '/' },
  { exact: false, component: NotFound, path: '*' },
];
